#pragma once

#include <cstdio>
#include<stdexcept>

template<class T>
class SimpleAllocator {
public:
    /// Function that's allocating array in heap with size = 'size'. Returns pointer to it.
    static T *allocate(std::size_t size = 1) {
        if (size == 0) {
            throw std::bad_alloc();
        }
        T *ptr = (T *) ::operator new(size * sizeof(T));
        return ptr;
    }

    /// Function that's realising data at address given pointer with size = 'size'
    static void deallocate(T *ptr, std::size_t size = 1) {
        if (ptr == nullptr && size == 0) {
            return;
        }
        if (ptr == nullptr && size != 0) {
            throw std::invalid_argument("Can't deallocate a nullptr with size different from zero!");
        }
        if (size == 0) {
            throw std::invalid_argument("Can't deallocate valid pointer with size of zero!");
        }
        ::operator delete(ptr, size * sizeof(T));
    }

    /// Function that's constructing object in place
    template<class... Args>
    void construct(T *p, Args &&... args) {
        ::new((T *) p) T(std::forward<Args>(args)...);
    }


    SimpleAllocator(const SimpleAllocator &a) = default;

    SimpleAllocator &operator=(const SimpleAllocator &al) = default;

    SimpleAllocator() = default;

    ~SimpleAllocator() = default;
};