#pragma once

#include <unordered_set>
#include "SimpleAllocator.h"
#include<utility>
#include <iostream>

class hash_pair;

template<class T>
using mypair = std::pair<T*, std::size_t>;

template<class T>
using set = std::unordered_set<mypair<T>,hash_pair>;

/// Struct with only one operator() that's computing hash of a given pair.
class hash_pair {
public:
    template <class T1, class T2>
    size_t operator()(const std::pair<T1, T2>& p) const
    {
        auto hash1 = std::hash<T1>{}(p.first);
        auto hash2 = std::hash<T2>{}(p.second);
        return hash1 ^ hash2;
    }
};



/**
 * @page Allocator
 * This allocator is using previously declared SimpleAllocator.h and it's used for more accurate and in depth testing.
 */
template <class T>
class DebugAllocator {
private:
    /// Set of all acquired addresses by allocator
    set<T> acquiredAddresses = set<T>();
    /// Number of acquired addresses
    std::size_t acquiredAddressesCount = 0;
public:
    /// Allocate one address with size = 'size' and book it in set
    T *allocate(std::size_t size = 1) {
        T *ptr;
        try {
            ptr = SimpleAllocator<T>::allocate(size);
        } catch (const std::bad_alloc &err) {
            std::cerr << err.what();
            throw err;
        }
        acquiredAddressesCount++;
        acquiredAddresses.emplace(std::make_pair(ptr, size));
        return ptr;
    }
    /// Deallocate data at given pointer and size and also record this is set.
    void deallocate(T *ptr, std::size_t size = 1) {
        if (ptr == nullptr) {
            throw std::invalid_argument("Given pointer is not a valid one!");
        }
        const auto addr = acquiredAddresses.find(std::make_pair(ptr, size));
        if(addr == this->acquiredAddresses.end()) {
            throw std::runtime_error("Address is not found in a set of allocated ones!");
        }
        if (addr->second != size) {
            throw std::runtime_error("Either given m_size is mistaken or there is runtime error!");
        }
        try {
            SimpleAllocator<T>::deallocate(addr->first, addr->second);
        } catch (const std::invalid_argument &e) {
            std::cerr << e.what();
            throw;
        } catch (const std::exception &e) {
            std::cerr << e.what();
            throw;
        }
        acquiredAddressesCount--;
        acquiredAddresses.erase(addr);
    }
    /// Deallocate all acquired addresses booked in acquiredAddresses set.
    /// Function that's constructing object in place
    template<class... Args>
    void construct(T *ptr, Args &&... args) {
        try {
            ptr = SimpleAllocator<T>::construct(args ... );
        } catch (const std::bad_alloc &err) {
            std::cerr << err.what();
            throw err;
        }
        acquiredAddressesCount++;
        acquiredAddresses.emplace(std::make_pair(ptr, sizeof(T)));
    }
    void deallocateAll() {
        for (const auto &addr : acquiredAddresses) {
            try {
                SimpleAllocator<T>::deallocate(addr.first, addr.second);
            } catch (const std::invalid_argument &e) {
                std::cerr << e.what();
                throw e;
            } catch (const std::exception &e) {
                std::cerr << e.what();
                throw e;
            }
        }
        acquiredAddresses.clear();
        acquiredAddressesCount = 0;
    }
    /// Returns const reference to booking set acquiredAddresses.
    [[nodiscard]] const set<T> &getAcquiredAddresses() const noexcept {
        return acquiredAddresses;
    }
    /// Returns number of acquired addresses.
    [[nodiscard]] std::size_t getAcquiredAddressesCount() const noexcept {
        return acquiredAddressesCount;
    }

    DebugAllocator(const DebugAllocator &a) = default;

    DebugAllocator &operator=(const DebugAllocator &al) = default;

    DebugAllocator() = default;

    ~DebugAllocator() = default;
};