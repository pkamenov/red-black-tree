#include "Node.h"
#include "catch2/catch_all.hpp"

SCENARIO("Creating nodes with constructor and checking their members.", "[node]") {
	WHEN("You create a Node with default constructor") {
		Node<long> node;
		THEN("Data is 0 and children and parent are nullptr. Also isLeaf and isRoot") {
			REQUIRE(node.getData() == 0);
			REQUIRE(node.getChild(LEFT) == NIL);
			REQUIRE(node.getChild(RIGHT) == NIL);
			REQUIRE(node.getParent() == NIL);
			REQUIRE(node.isLeaf());
			REQUIRE(node.isRoot());
		}
		THEN("Sibling is NIL, grandparent is NIL and child direction throws") {
			REQUIRE(node.getSibling() == NIL);
			REQUIRE(node.getGrandParent() == NIL);
			REQUIRE_THROWS_AS(node.childDir(), std::invalid_argument);
		}
	}

	GIVEN("Three nodes and connect them") {
		Node<long> parent(5, BLACK), left(6, RED, &parent, NIL, NIL), right(7, RED, &parent, NIL, NIL);
		parent.setChild(LEFT, &left);
		parent.setChild(RIGHT, &right);
		THEN("Sibling of left is right and sibling of right is left") {
			REQUIRE(&left == right.getSibling());
			REQUIRE(&right == left.getSibling());
		}
		THEN("Is leaf of left right is true") {
			REQUIRE(left.isLeaf());
			REQUIRE(right.isLeaf());
		}
		THEN("Root is not leaf") {
			REQUIRE_FALSE(parent.isLeaf());
		}
		THEN("Grandparent is NIL") {
			REQUIRE(left.getGrandParent() == NIL);
		}
		THEN("Nephews of leafs are NIL") {
			REQUIRE(left.getCloseNephew() == NIL);
			REQUIRE(right.getFarNephew() == NIL);
		}
		THEN("Root uncle is NIL") {
			REQUIRE(parent.getUncle() == NIL);
		}
		THEN("Root sibling is NIL") {
			REQUIRE(parent.getSibling() == NIL);
		}


		WHEN("You add one new element as left child of right") {
			Node<long> rightRight(8, BLACK, &right, NIL, NIL);
			Node<long> rightLeft(7, RED, &right, NIL, NIL);
			right.setChild(LEFT, &rightLeft);
			right.setChild(RIGHT, &rightRight);
			THEN("New node RightLeft is leaf with parent right and grandparent root, sibling leftRight and uncle is left") {
				REQUIRE(rightLeft.isLeaf());
				REQUIRE(rightRight.getParent() == &right);
				REQUIRE(rightRight.getGrandParent() == &parent);
				REQUIRE(rightRight.getSibling() == &rightLeft);
				REQUIRE(rightRight.getUncle() == &left);
			}

			THEN("Left close nephew is rightLeft and far rightRight") {
				REQUIRE(left.getCloseNephew() == &rightLeft);
				REQUIRE(left.getFarNephew() == &rightRight);
			}
		}
	}

	GIVEN("Two equal nodes") {
		Node<long> parentOne(1, RED, NIL, NIL, NIL);
		Node<long> leftOne(2, BLACK, &parentOne, NIL, NIL);
		Node<long> rightOne(3, BLACK, &parentOne, NIL, NIL);
		Node<long> parentTwo(1, RED, NIL, NIL, NIL);
		Node<long> leftTwo(2, BLACK, &parentTwo, NIL, NIL);
		Node<long> rightTwo(3, BLACK, &parentTwo, NIL, NIL);
		parentOne.setChild(LEFT, &leftOne);
		parentOne.setChild(RIGHT, &rightOne);
		parentTwo.setChild(LEFT, &leftTwo);
		parentTwo.setChild(RIGHT, &rightTwo);
		WHEN("You use operator == ") {
			THEN("It returns true") {
				REQUIRE(parentOne == parentTwo);
			}
		}
		WHEN("You use operator != ") {
			THEN("It returns false") {
				REQUIRE_FALSE(parentOne != parentTwo);
			}
		}
		WHEN("You change the data of node") {
			parentOne.setData(8);
			THEN("Operator == returns false") {
				REQUIRE_FALSE(parentOne == parentTwo);
			}
		}
		WHEN("You change the data of child node") {
			leftOne.setData(8);
			THEN("Operator == returns false") {
				REQUIRE_FALSE(parentOne == parentTwo);
			}
		}
		WHEN("You change the color of node") {
			parentOne.setColor(BLACK);
			THEN("Operator == returns false") {
				REQUIRE_FALSE(parentOne == parentTwo);
			}
		}
		WHEN("You change the color of  child node") {
			leftOne.setColor(RED);
			THEN("Operator == returns false") {
				REQUIRE_FALSE(parentOne == parentTwo);
			}
		}
		WHEN("Add one child on the two equal nodes") {
			Node<long> leftLeft(4, BLACK, &parentOne, NIL, NIL);
			leftOne.setChild(LEFT, &leftLeft);
			THEN("Operator == returns false") {
				REQUIRE_FALSE(parentOne == parentTwo);
			}
		}

		WHEN("You change the color of node") {
			parentOne.setColor(BLACK);
			THEN("Operator != returns true") {
				REQUIRE(parentOne != parentTwo);
			}
		}
	}
}