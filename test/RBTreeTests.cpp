#include "catch2/catch_all.hpp"
#include"RBTree.h"
#include"DebugAllocator.h" 
#include"RBTreeHelper.h"
#include"cpp.hint"
#include<algorithm>
#include<cmath>
#include<algorithm>
#include<fstream>

SCENARIO("Testing tree", "[tree]") {
	GIVEN("Tree with 7 nodes") {
		Node<short> parent(1, BLACK, NIL, NIL, NIL);
		Node<short> leftChild(2, BLACK, &parent, NIL, NIL);
		Node<short> rightChild(3, BLACK, &parent, NIL, NIL);
		parent.setChild(LEFT, &leftChild);
		parent.setChild(RIGHT, &rightChild);
		Node<short> leftLeftChild(4, BLACK, &leftChild, NIL, NIL);
		Node<short> leftRightChild(5, BLACK, &leftChild, NIL, NIL);
		leftChild.setChild(LEFT, &leftLeftChild);
		leftChild.setChild(RIGHT, &leftRightChild);
		Node<short> rightLeftChild(6, BLACK, &rightChild, NIL, NIL);
		Node<short> rightRightChild(7, BLACK, &leftChild, NIL, NIL);
		rightChild.setChild(LEFT, &rightLeftChild);
		rightChild.setChild(RIGHT, &rightRightChild);
		RBTree<short> tree;
		tree.insert(5); tree.insert(3); tree.insert(8); tree.insert(2);
		tree.insert(4); tree.insert(7); tree.insert(9);
		WHEN("Calculating height of given tree ") {
			THEN("Height is 2") {
				REQUIRE(tree.height() == 2);
			}
		}
		WHEN("Calculating height of given tree ") {
			THEN("Height is 2") {
				REQUIRE(tree.height() == 2);
			}
		}
		WHEN("You add a new node") {
			tree.insert(10);
			THEN("Height is increased by one") {
				REQUIRE(tree.height() == 3);
			}
		}
		WHEN("You use contains() with element that is in the tree") {
			THEN("Contains returns true") {
				REQUIRE(tree.contains(3));
			}
		}
		WHEN("You use contains() with element that is not in the tree") {
			THEN("Contains returns true") {
				REQUIRE(!tree.contains(10));
			}
		}
	}
	GIVEN("Tree with allocator") {
		RBTree<short, DebugAllocator<Node<short>>> tree;
		WHEN("There is an empty tree") {
			THEN("There are not allocated elements") {
				REQUIRE(tree.get_allocator().getAcquiredAddressesCount() == tree.size());
			}
		}
		WHEN("There is tree with 3 elements") {
			tree.insert(5);
			tree.insert(3);
			tree.insert(8);
			THEN("There are 3 elements allocated") {
				REQUIRE(tree.get_allocator().getAcquiredAddressesCount() == tree.size());
			}
			THEN("Size of tree is 3") {
				REQUIRE(tree.size() == 3);
			}
		}
		WHEN("There is tree with 3 elements and we call clear function one of them") {
			tree.insert(5);
			tree.insert(3);
			tree.insert(8);
			tree.erase(8);
			THEN("There are 2 elements allocated") {
				REQUIRE(tree.get_allocator().getAcquiredAddressesCount() == tree.size());
			}
		}
		WHEN("You use erase function to clear all elements") {
			tree.insert(5);
			tree.insert(3);
			tree.insert(8);
			tree.erase(3);
			REQUIRE(tree.get_allocator().getAcquiredAddressesCount() == 2);
			tree.erase(5);
			REQUIRE(tree.get_allocator().getAcquiredAddressesCount() == 1);
			tree.erase(8);
			THEN("Allocated elements are 0") {
				REQUIRE(tree.get_allocator().getAcquiredAddressesCount() == 0);
			}
			THEN("Size is 0") {
				REQUIRE(tree.size() == 0);
			}
		}
		WHEN("You use toSortedVector function") {
			RBTree<short> tree;
			tree.insert(5); tree.insert(3); tree.insert(8); tree.insert(2);
			tree.insert(4); tree.insert(7); tree.insert(9);
			std::vector<short> vector = tree.toSortedVector();
			THEN("Size of vector is size of tree") {
				REQUIRE(vector.size() == tree.size());
			}
			THEN("Elements of vector are sorted is vector") {
				std::vector<short> result({ 2,3,4,5,7,8,9 });
				REQUIRE(vector == result);
			}
		}
	}

	GIVEN("An empty tree") {
		RBTree<short> tree;
		WHEN("Calculating height of tree") {
			THEN("Height is -1") {
				REQUIRE(tree.height() == -1);
			}
		}
		WHEN("Calling empty function") {
			THEN("Returns true") {
				REQUIRE(tree.empty() == true);
			}
		}
		WHEN("Calling contains element") {
			THEN("Returns false") {
				REQUIRE_FALSE(tree.contains(6));
			}
		}
		WHEN("you try to erase something") {
			THEN("Nothing happens") {
				REQUIRE_THROWS(tree.erase(23345));
			}
		}
	}
}

SCENARIO("Testing is red black tree functions", "[is_red_black_tree]") {
	GIVEN("a nil") {
		Node<short>* node = NIL;
		THEN("is red black tree") {
			REQUIRE(RBTreeHelper<short>::isRedBlackTree(node));
		}
	}

	GIVEN("an example 3 node red black tree") {
		Node<long> root(10, BLACK), left(15, RED, &root), right(5, RED, &root);
		root.setChild(LEFT, &left); root.setChild(RIGHT, &right);
		THEN("every path to NIL has the same number of black nodes") {
			REQUIRE(RBTreeHelper<long>::everyPathToNILHasSameNumberOfBlackNodes(&root));
		}

		THEN("every red nodes does not have red children") {
			REQUIRE(RBTreeHelper<long>::allRedNodesHasBlackChildren(&root));
		}
		WHEN("you add two more child to each left leaf and one to right") {
			Node<long> leftLeft(1, BLACK, &left), leftRight(6, BLACK, &left), rightLeft(11, BLACK, &right);
			left.setChild(LEFT, &leftLeft); left.setChild(RIGHT, &leftRight); right.setChild(LEFT, &rightLeft);
			THEN("every path to NIL has not the same number of black nodes") {
				REQUIRE_FALSE(RBTreeHelper<long>::everyPathToNILHasSameNumberOfBlackNodes(&root));
			}
			THEN("every red nodes does not have red children") {
				REQUIRE(RBTreeHelper<long>::allRedNodesHasBlackChildren(&root));
			}
			WHEN("you add a red node to 15(red node)") {
				Node<long> rihgtRight(20, RED, &right);
				right.setChild(RIGHT, &rihgtRight);
				THEN("Red property is broken") {
					REQUIRE_FALSE(RBTreeHelper<long>::allRedNodesHasBlackChildren(&root));
				}
				THEN("black property is broken") {
					REQUIRE_FALSE(RBTreeHelper<long>::everyPathToNILHasSameNumberOfBlackNodes(&root));
				}
			}
			WHEN("you add a black node to 15") {
				Node<long> rihgtRight(20, BLACK, &right);
				right.setChild(RIGHT, &rihgtRight);
				THEN("Red property is valid") {
					REQUIRE(RBTreeHelper<long>::allRedNodesHasBlackChildren(&root));
				}
				THEN("black property is valid") {
					REQUIRE(RBTreeHelper<long>::everyPathToNILHasSameNumberOfBlackNodes(&root));
				}
				WHEN("you add one more red node to right side of 6") {
					Node<long> leftRightRight(8, RED, &leftRight);
					leftRight.setChild(RIGHT, &leftRightRight);
					THEN("Red property is valid") {
						REQUIRE(RBTreeHelper<long>::allRedNodesHasBlackChildren(&root));
					}
					THEN("black property is valid") {
						REQUIRE(RBTreeHelper<long>::everyPathToNILHasSameNumberOfBlackNodes(&root));
					}
				}
			}
		}
	}
}

SCENARIO("Tests for Left and right rotations", "[rotations]") {
	GIVEN("A tree with 7 elements") {
		Node<int> root(0, BLACK), child1(1, BLACK, &root), child2(2, BLACK, &root),
			child3(3, BLACK, &child1), child4(4, BLACK, &child1),
			child5(5, BLACK, &child3), child6(6, BLACK, &child3);
		root.setChild(LEFT, &child1);
		root.setChild(RIGHT, &child2);
		child1.setChild(LEFT, &child3);
		child1.setChild(RIGHT, &child4);
		child2.setChild(LEFT, &child5);
		child2.setChild(RIGHT, &child6);
		RBTree<int, SimpleAllocator<Node<int>>>* t = new RBTree<int, SimpleAllocator<Node<int>>>;
		WHEN("you rotate the tree to left") {
			Node<int>* result = Rotations<int, SimpleAllocator<Node<int>>>::rotateDir((*t), &root, LEFT);
			THEN("Tree is as expected") {
				Node<int> newroot(2, BLACK), newchild1(0, BLACK, &newroot), newchild2(6, BLACK, &newroot),
					newchild3(1, BLACK, &newchild1), newchild4(5, BLACK, &newchild1),
					newchild5(3, BLACK, &newchild3), newchild6(4, BLACK, &newchild3);
				newroot.setChild(LEFT, &newchild1);
				newroot.setChild(RIGHT, &newchild2);
				newchild1.setChild(LEFT, &newchild3);
				newchild1.setChild(RIGHT, &newchild4);
				newchild3.setChild(LEFT, &newchild5);
				newchild3.setChild(RIGHT, &newchild6);
				REQUIRE((*result) == newroot);
			}
		}

		WHEN("you rotate the tree to right") {
			Node<int>* result = Rotations<int, SimpleAllocator<Node<int>>>::rotateDir((*t), &root, RIGHT);
			THEN("Tree is as expected") {
				Node<int> newroot(1, BLACK), newchild1(3, BLACK, &newroot), newchild2(0, BLACK, &newroot),
					newchild3(4, BLACK, &newchild2), newchild4(2, BLACK, &newchild2),
					newchild5(5, BLACK, &newchild4), newchild6(6, BLACK, &newchild4);
				newroot.setChild(LEFT, &newchild1);
				newroot.setChild(RIGHT, &newchild2);
				newchild2.setChild(LEFT, &newchild3);
				newchild2.setChild(RIGHT, &newchild4);
				newchild4.setChild(LEFT, &newchild5);
				newchild4.setChild(RIGHT, &newchild6);
				REQUIRE((*result) == newroot);
			}
		}
	}
}

template <class T = long, class Allocator = SimpleAllocator<Node<T>>>
void isTreeInOrder(const RBTree<T, Allocator>& t) {
	std::vector<T> vec = t.toSortedVector();
	REQUIRE(std::is_sorted(vec.begin(), vec.end()));
}

template<class T = long, class Allocator = SimpleAllocator<Node<T>> >
void treeKeepsSameElementsAfterInsertion(const RBTree<T, Allocator>& t, std::vector<T>& vec) {
	std::sort(vec.begin(), vec.end());
	std::vector<T> vecTree = t.toSortedVector();
	REQUIRE(vecTree == vec);
}

template <class T = long, class Allocator = SimpleAllocator<Node<T>>>
void isRedBlackTree(const RBTree<T, Allocator>& t) {
	REQUIRE(RBTreeHelper<T>::isRedBlackTree(t.getRoot()));
}

template <class T = long, class Allocator = SimpleAllocator<Node<T>>>
void isValid(const RBTree<T, Allocator>& t, std::vector<T>& vec) {
	isTreeInOrder<T, Allocator>(t);
	treeKeepsSameElementsAfterInsertion<T, Allocator>(t, vec);
	isRedBlackTree<T, Allocator>(t);
	REQUIRE(t.size() == vec.size());
}

template<class T = long>
void removeElement(std::vector<T>& vec, T elem) {
	for (std::size_t i = 0; i < vec.size(); ++i) {
		if (vec[i] == elem) {
			vec.erase(vec.begin() + i);
		}
	}
}

double log2(std::size_t n) {
	double first = log(n);
	double second = log(2);
	return first / second;
}

SCENARIO("Testing validity of RBTree after insertion", "[insert]") {
	GIVEN("RandomTests: An empty tree") {
		RBTree<short> t;
		std::vector<short> v;
		WHEN("You add one element") {
			v.emplace_back(5);
			t.insert(5);
			THEN("Tree is in valid state") {
				isValid(t, v);
			}
			WHEN("You add 5 shuffled elements") {
				v.push_back(3); v.push_back(1); v.push_back(4); v.push_back(2);
				t.insert(3); t.insert(1); t.insert(4); t.insert(2);
				THEN("Tree is in valid state") {
					isValid(t, v);
				}
			}
		}

		WHEN("You add 10000 random elements") {
			DEFINE_LARGE_VECTOR_WITH_RANDOM_NUMBERS_1;
			for (size_t i = 0; i < l1.size(); ++i) {
				t.insert(l1[i]);
			}
			THEN("Tree is in valid state") {
				isValid(t, l1);
			}
			THEN("Tree height is around log of size") {
				std::size_t height = t.height();
				std::size_t size = t.size();
				REQUIRE(height < size);
				REQUIRE(height < 2 *
					log2(size / 2 + 1));
			}
		}
	}

	GIVEN("Insertion Case 1: A tree with 4 node. Parent of newly inserted one is black. Just insert") {
		RBTree<int> t;
		t.insert(5);
		REQUIRE(t.getRoot()->getColor() == RED);
		t.insert(6); t.insert(8); t.insert(10);
		REQUIRE(t.getRoot()->getColor() == RED);
		REQUIRE(t.getRoot()->getChild(LEFT)->getColor() == BLACK);
		WHEN("You add a red child to a black parent") {
			RBTreeIterator newNode = t.insert(4);
			REQUIRE(newNode->getParent()->getColor() == BLACK);
			REQUIRE(newNode->getColor() == RED);
			THEN("After insertion tree is a valid red-black one") {
				isRedBlackTree<int>(t);
			}
		}
	}

	GIVEN("Insertion case 2: Both parent and uncle of currently inserted one are red") {
		RBTree t;
		t.insert(5);
		t.insert(3);
		t.insert(8);
		const Node<long>* root = t.getRoot();
		const Node<long>* parent = root->getChild(LEFT);
		const Node<long>* uncle = root->getChild(RIGHT);
		REQUIRE(root->getColor() == BLACK);
		REQUIRE(parent->getColor() == RED);
		REQUIRE(uncle->getColor() == RED);
		WHEN("You insert the new node") {
			RBTreeIterator newNode = t.insert(2);
			THEN("Then new node is red, parent and uncle are repainted\
				black and grandparent(root) is red. Also tree is a valid one.") {
				REQUIRE(root->getColor() == RED);
				REQUIRE(parent->getColor() == BLACK);
				REQUIRE(uncle->getColor() == BLACK);
				REQUIRE(newNode->getColor() == RED);
				isRedBlackTree(t);
			}
		}
	}

	GIVEN("Insertion case 3: An empty tree") {
		RBTree t;
		WHEN("You insert a new node to the empty tree") {
			auto newNode = t.insert(1503);
			THEN("Tree is in valid state and newly inserted node is red") {
				REQUIRE(newNode->getColor() == RED);
				isRedBlackTree(t);
			}
		}

	}

	GIVEN("Insertion case 4: The parent is red and root") {
		RBTree<int> t(2);
		REQUIRE(t.getRoot()->getColor() == RED);
		WHEN("You add right element to root") {
			auto newNode = t.insert(10);
			THEN("New element is red, root is black and tree is a valid one") {
				REQUIRE(newNode->getColor() == RED);
				REQUIRE(t.getRoot()->getColor() == BLACK);
				isRedBlackTree(t);
			}
		}
	}

	GIVEN("Insertion case 5: The parent is red, but the uncle is black. New node will be inner grandchild after inserting.") {
		RBTree<short>* t = new RBTree<short>;
		Node<short> grandParent(10, BLACK), parent(5, RED, &grandParent), uncle(15, BLACK, &grandParent);
		grandParent.setChild(LEFT, &parent);
		grandParent.setChild(RIGHT, &uncle);
		WHEN("You insert a new child left at parent") {
			Node<short> n(8, RED, &parent);
			parent.setChild(RIGHT, &n);
			Rotations<short>::rebalance(*t, &n);
			THEN("New node is red. His sibling is also red. Far nephew is black") {
				REQUIRE(n.getColor() == BLACK);
				REQUIRE(parent.getColor() == RED);
				REQUIRE(parent.getSibling()->getColor() == RED);
				REQUIRE(parent.getFarNephew()->getColor() == BLACK);
			}
		}
	}

	GIVEN("Insertion case 6: The parent is red, but the uncle is black. New node will be outer grandchild after inserting.") {
		RBTree<short>* t = new RBTree<short>;
		Node<short> grandParent(10, BLACK), parent(5, RED, &grandParent), uncle(15, BLACK, &grandParent);
		grandParent.setChild(LEFT, &parent);
		grandParent.setChild(RIGHT, &uncle);
		WHEN("You insert a new child left at parent") {
			Node<short> n(8, RED, &parent);
			parent.setChild(LEFT, &n);
			Rotations<short>::rebalance(*t, &n);
			THEN("New node is red. His sibling is also red. Far nephew is black") {
				REQUIRE(n.getColor() == RED);
				REQUIRE(parent.getColor() == BLACK);
				REQUIRE(n.getSibling()->getColor() == RED);
				REQUIRE(n.getFarNephew()->getColor() == BLACK);
			}
		}
	}
}

SCENARIO("Testing validity of RBTree after erase", "[erase]") {
	GIVEN("An empty tree. Random tests") {
		RBTree<short, DebugAllocator<Node<short>>> t;
		std::vector<short> v;
		WHEN("You erase an element from the empty tree") {
			REQUIRE_THROWS(t.erase(5));
			THEN("Tree is in valid state and it's not changed") {
				isValid<short, DebugAllocator<Node<short>>>(t, v);
				REQUIRE(t.size() == 0);
			}
			WHEN("You add 4 shuffled elements") {
				v.push_back(3); v.push_back(1); v.push_back(4); v.push_back(2);
				t.insert(3); t.insert(1); t.insert(4); t.insert(2);
				THEN("Tree is in valid state") {
					isValid<short, DebugAllocator<Node<short>>>(t, v);
				}
				WHEN("you erase 2 elements from the tree") {
					t.erase(3);
					t.erase(2);
					removeElement<short>(v, 3); removeElement<short>(v, 2);
					THEN("Tree is in valid state") {
						isValid<short, DebugAllocator<Node<short>>>(t, v);
					}
				}
			}

			WHEN("You add 20'000 random elements") {
				DEFINE_LARGE_VECTOR_WITH_RANDOM_NUMBERS_1;
				std::size_t size = l1.size();
				for (size_t i = 0; i < l1.size(); ++i) {
					t.insert(l1[i]);
				}
				THEN("Tree is in valid state") {
					isValid<short, DebugAllocator<Node<short>>>(t, l1);
				}
				WHEN("you erase 5000 from this random elements") {
					std::size_t to = 5000;
					for (size_t i = 0; i < to; ++i) {
						t.erase(l1[i]);
						l1.erase(l1.begin() + i);
						if (i % 100 == 0) {
							REQUIRE(t.get_allocator().getAcquiredAddressesCount() == size - (i + 1));
						}
					}
					THEN("Tree is still in valid state") {
						isValid<short, DebugAllocator<Node<short>>>(t, l1);
						REQUIRE(t.get_allocator().getAcquiredAddressesCount() == size - to);
					}
					THEN("Tree height is around log of size") {
						std::size_t height = t.height();
						std::size_t size = t.size();
						REQUIRE(height < size);
						REQUIRE(height < 2 *
							log2(size / 2 + 1));
					}
				}
			}
		}
	}

	GIVEN("Simple case 1: Tree with only one element. Deleting the root.") {
		RBTree<short> t(5);
		WHEN("You delete the root") {
			t.erase(5);
			THEN("Tree is empty and valid.") {
				REQUIRE(t.empty());
				isRedBlackTree(t);
			}
		}
	}
	GIVEN("Simple case 2: Root with two children. Deleting the root.") {
		RBTree t(10);
		t.insert(8);
		t.insert(108);
		WHEN("You delete the root") {
			t.erase(10);
			THEN("Tree is valid") {
				isRedBlackTree(t);
				REQUIRE(t.getRoot()->getData() == 8);
				REQUIRE(t.getRoot()->getColor() == BLACK);
			}
		}
	}
	GIVEN("Simple case 3: N is a red node. Without children. It's not the root") {
		RBTree t(10);
		auto n = t.insert(8);
		REQUIRE(t.getRoot()->getColor() == BLACK);
		REQUIRE(n->getColor() == RED);
		WHEN("You delete n") {
			t.erase(n);
			THEN("Tree is valid") {
				isRedBlackTree(t);
				REQUIRE(t.getRoot()->getData() == 10);
				REQUIRE(t.getRoot()->getColor() == BLACK);
			}
		}
	}
	GIVEN("Simple case 4.1: N is black node without children.It's not the root") {
		RBTree<long>* t = new RBTree<long>;
		Node<long> root(10, RED), leftChild(8, BLACK, &root), rightChild(15, BLACK, &root);
		root.setChild(LEFT, &leftChild);
		root.setChild(RIGHT, &rightChild);
		Node<long>* ptr = &leftChild;
		WHEN("You erase leftChild which is black node, without children") {
			Rotations<long>::eraseRebalance(*t, ptr);
			THEN("Tree is in valid state") {
				REQUIRE(root.getColor() == BLACK);
				REQUIRE(root.getData() == 10);
				REQUIRE(rightChild.getColor() == RED);
				REQUIRE(rightChild.getData() == 15);
				REQUIRE(RBTreeHelper<long>::isRedBlackTree(&root));
			}
		}
	}
	GIVEN("Simple case 4.2: N is black node with one red child.") {
		RBTree<long>* t = new RBTree<long>;
		Node<long> root(10, RED), leftChild(8, BLACK, &root), rightChild(15, BLACK, &root);
		root.setChild(LEFT, &leftChild);
		root.setChild(RIGHT, &rightChild);
		Node<long> leftLeftChild(7, RED, &leftChild);
		leftChild.setChild(LEFT, &leftLeftChild);
		Node<long>* ptr = &leftChild;
		WHEN("You erase leftChild which is black node, without children") {
			Rotations<long>::eraseRebalance(*t, ptr);
			THEN("Tree is in valid state") {
				REQUIRE(root.getColor() == RED);
				REQUIRE(root.getData() == 10);
				REQUIRE(rightChild.getColor() == BLACK);
				REQUIRE(rightChild.getData() == 15);
				REQUIRE(root.getChild(LEFT)->getColor() == BLACK);
				REQUIRE(root.getChild(LEFT)->getData() == 7);
				REQUIRE(RBTreeHelper<long>::isRedBlackTree(&root));
			}
		}
	}
	GIVEN("Erase case 1: N is non root leaf. Parent is black. Sibling and Sibling's children are black") {
		RBTree<long>* t = new RBTree<long>;
		Node<long> root(10, BLACK), leftChild(7, BLACK, &root), rightChild(15, BLACK, &root),
			rightLeft(12, BLACK, &rightChild), rightRight(18, BLACK, &rightChild);
		root.setChild(LEFT, &leftChild);
		root.setChild(RIGHT, &rightChild);
		rightChild.setChild(LEFT, &rightLeft);
		rightChild.setChild(RIGHT, &rightRight);
		Node<long>* ptr = &leftChild;
		WHEN("You erase leftChild which is black node, without children") {
			Rotations<long>::deleteBlackNonRootLeaf(*t, ptr);
			THEN("Tree is in valid state") {
				REQUIRE(root.getColor() == BLACK);
				REQUIRE(root.getData() == 10);
				REQUIRE(rightChild.getColor() == RED);
				REQUIRE(rightChild.getData() == 15);
			}
		}
	}
	GIVEN("Erase case 2: Test with example tree in witch deletion we enter the while loop") {
		RBTree t(10);
		for (long i = 11; i < 150; ++i)
			t.insert(i);
		WHEN("You erase 85, a leaf") {
			t.erase(10);
			THEN("Tree is in valid state") {
				isRedBlackTree(t);
			}
		}
	}
	GIVEN("Erase case 3: N is black non root leaf. Sibling is red. Parent and both nephew are black.") {
		RBTree<long>* t = new RBTree<long>;
		Node<long> root(10, BLACK), leftChild(5, BLACK, &root), rightChild(15, RED, &root),
			rightLeft(12, BLACK, &rightChild), rightRight(18, BLACK, &rightChild);
		root.setChild(LEFT, &leftChild);
		root.setChild(RIGHT, &rightChild);
		rightChild.setChild(LEFT, &rightLeft);
		rightChild.setChild(RIGHT, &rightRight);
		Node<long>* ptr = &leftChild;
		WHEN("You erase leftChild which is black node, without children. With red sibling and both nephews black") {
			Rotations<long>::deleteBlackNonRootLeaf(*t, ptr);
			THEN("Tree is in valid state") {
				REQUIRE(RBTreeHelper<long>::isRedBlackTree(&root));
			}
		}
	}
	GIVEN("Erase case 4: N is black non root leaf. Sibling is black. Parent is red. Both nephew are black.") {
		RBTree<long>* t = new RBTree<long>;
		Node<long> root(10, RED), leftChild(5, BLACK, &root), rightChild(15, BLACK, &root),
			rightLeft(12, BLACK, &rightChild), rightRight(18, BLACK, &rightChild);
		root.setChild(LEFT, &leftChild);
		root.setChild(RIGHT, &rightChild);
		rightChild.setChild(LEFT, &rightLeft);
		rightChild.setChild(RIGHT, &rightRight);
		Node<long>* ptr = &leftChild;
		WHEN("You erase leftChild which is black node, without children. With red sibling and both nephews black") {
			Rotations<long>::deleteBlackNonRootLeaf(*t, ptr);
			THEN("Tree is in valid state") {
				REQUIRE(root.getColor() == BLACK);
				REQUIRE(rightChild.getColor() == RED);
				REQUIRE(rightRight.getColor() == BLACK);
				REQUIRE(rightLeft.getColor() == BLACK);
			}
		}
	}
	GIVEN("Erase case 5.1: N is black non root leaf. Sibling is black. Parent is red. Close nephew is red, far nephew is black") {
		RBTree<long>* t = new RBTree<long>;
		Node<long> root(10, RED), leftChild(5, BLACK, &root), rightChild(15, BLACK, &root),
			rightLeft(12, RED, &rightChild), rightRight(18, BLACK, &rightChild);
		root.setChild(LEFT, &leftChild);
		root.setChild(RIGHT, &rightChild);
		rightChild.setChild(LEFT, &rightLeft);
		rightChild.setChild(RIGHT, &rightRight);
		Node<long>* ptr = &leftChild;
		WHEN("You erase leftChild which is black node, without children. With red sibling and both nephews black") {
			Rotations<long>::deleteBlackNonRootLeaf(*t, ptr);
			THEN("Tree is in valid state") {
				REQUIRE(RBTreeHelper<long>::isRedBlackTree(&root));
			}
		}
	}
	GIVEN("Erase case 5.2: N is black non root leaf. Sibling is black. Parent is black. Close nephew is red, far nephew is black") {
		RBTree<long>* t = new RBTree<long>;
		Node<long> root(10, BLACK), leftChild(5, BLACK, &root), rightChild(15, BLACK, &root),
			rightLeft(12, RED, &rightChild), rightRight(18, BLACK, &rightChild);
		root.setChild(LEFT, &leftChild);
		root.setChild(RIGHT, &rightChild);
		rightChild.setChild(LEFT, &rightLeft);
		rightChild.setChild(RIGHT, &rightRight);
		Node<long>* ptr = &leftChild;
		WHEN("You erase leftChild which is black node, without children. With red sibling and both nephews black") {
			Rotations<long>::deleteBlackNonRootLeaf(*t, ptr);
			THEN("Tree is in valid state") {
				REQUIRE(RBTreeHelper<long>::isRedBlackTree(&root));
			}
		}
	}
	GIVEN("Erase case 6.1: N is black non root leaf. Sibling is black. Parent is black. Close nephew is black, far nephew is red") {
		RBTree<long>* t = new RBTree<long>;
		Node<long> root(10, BLACK), leftChild(5, BLACK, &root), rightChild(15, BLACK, &root),
			rightLeft(12, BLACK, &rightChild), rightRight(18, RED, &rightChild);
		root.setChild(LEFT, &leftChild);
		root.setChild(RIGHT, &rightChild);
		rightChild.setChild(LEFT, &rightLeft);
		rightChild.setChild(RIGHT, &rightRight);
		Node<long>* ptr = &leftChild;
		WHEN("You erase leftChild which is black node, without children. With red sibling and both nephews black") {
			Rotations<long>::deleteBlackNonRootLeaf(*t, ptr);
			THEN("Tree is in valid state") {
				REQUIRE(root.getColor() == BLACK);
				REQUIRE(rightChild.getColor() == BLACK);
				REQUIRE(rightRight.getColor() == BLACK);
				REQUIRE(rightLeft.getColor() == BLACK);
			}
		}
	}
	GIVEN("Erase case 6.2: N is black non root leaf. Sibling is black. Parent is red. Close nephew is black, far nephew is red") {
		RBTree<long>* t = new RBTree<long>;
		Node<long> root(10, RED), leftChild(5, BLACK, &root), rightChild(15, BLACK, &root),
			rightLeft(12, BLACK, &rightChild), rightRight(18, RED, &rightChild);
		root.setChild(LEFT, &leftChild);
		root.setChild(RIGHT, &rightChild);
		rightChild.setChild(LEFT, &rightLeft);
		rightChild.setChild(RIGHT, &rightRight);
		Node<long>* ptr = &leftChild;
		WHEN("You erase leftChild which is black node, without children. With red sibling and both nephews black") {
			Rotations<long>::deleteBlackNonRootLeaf(*t, ptr);
			THEN("Tree is in valid state") {
				REQUIRE(root.getColor() == BLACK);
				REQUIRE(rightChild.getColor() == RED);
				REQUIRE(rightRight.getColor() == BLACK);
				REQUIRE(rightLeft.getColor() == BLACK);
			}
		}
	}
}

SCENARIO("Testing iterator for RBTree", "[iterator]") {
	GIVEN("An empty tree") {
		RBTree t;
		WHEN("You iterate the empty tree, forwards nothing happens") {
			for (const auto& i : t) {
				REQUIRE_NOTHROW(i.getData());
			}
		}

		WHEN("You iterate the empty tree, backwards nothing happens") {
			for (auto& iter = t.rbegin(); iter != t.rend(); --iter) {
				REQUIRE_NOTHROW(iter->getData());
			}
		}
	}

	GIVEN("An non empty tree, with 1000 elements") {
		RBTree<short> t;
		DEFINE_LARGE_VECTOR_WITH_RANDOM_NUMBERS_1;
		std::size_t size = l1.size();
		for (std::size_t i = 0; i < size; ++i) {
			t.insert(l1[i]);
		}
		std::sort(l1.begin(), l1.end());
		WHEN("You iterate the tree forward, everything works.") {
			std::size_t i = 0;
			for (const auto& el : t) {
				REQUIRE(el.getData() == l1[i++]);
			}
		}

		WHEN("You iterate the tree backwards, everything works.") {
			std::size_t i = 0;
			for (auto& iter = t.rbegin(); iter != t.rend(); --iter) {
				REQUIRE(iter->getData() == l1[size - (1 + i++)]);
			}
		}
	}
}