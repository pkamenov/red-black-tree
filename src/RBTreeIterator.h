#pragma once
#include"RBTree.h"
template<typename RBTree>
class RBTreeIterator {
public:
	using ValueType = typename RBTree::ValueType;
	using PointerType = ValueType*;
	using ReferenceType = ValueType&;

	explicit RBTreeIterator(PointerType _ptr = NIL) : ptr(_ptr) {}

	RBTreeIterator& operator++() {
		increment();
		return *this;
	}

	RBTreeIterator operator++(int) {
		RBTreeIterator iterator = *this;
		++(*this);
		return iterator;
	}

private:
	void increment() {
		if (ptr->getChild(RIGHT) == NIL && ptr->getParent() == NIL) {
			ptr = NIL;
			return;
		}
		if (ptr->getChild(RIGHT) != NIL) {
			ptr = ptr->getChild(RIGHT);
			while (ptr->getChild(LEFT) != NIL)
				ptr = ptr->getChild(LEFT);
		}
		else {
			PointerType parent = ptr->getParent();
			while (parent != NIL && ptr == parent->getChild(RIGHT)) {
				ptr = parent;
				parent = parent->getParent();
			}
			if (ptr->getChild(RIGHT) != parent)
				ptr = parent;
		}
	}

public:

	RBTreeIterator& operator--() {
		decrement();
		return *this;
	}

	RBTreeIterator operator--(int) {
		RBTreeIterator iterator = *this;
		--(*this);
		return iterator;
	}

private:
	void decrement() {
		if (ptr->getChild(LEFT) == NIL && ptr->getParent() == NIL) {
			ptr = NIL;
			return;
		}
		if (ptr->getChild(LEFT) != NIL) {
			ptr = ptr->getChild(LEFT);
			while (ptr->getChild(RIGHT) != NIL)
				ptr = ptr->getChild(RIGHT);
		}
		else {
			PointerType parent = ptr->getParent();
			while (parent != NIL && ptr == parent->getChild(LEFT)) {
				ptr = parent;
				parent = parent->getParent();
			}
			if (ptr->getChild(LEFT) != parent)
				ptr = parent;
		}
	}
public:

	PointerType operator->() {
		return ptr;
	}

	ReferenceType operator*() {
		return *ptr;
	}

	bool operator==(const RBTreeIterator& other) const {
		return ptr == other.ptr;
	}

	bool operator!=(const RBTreeIterator& other) const {
		return !(*this == other);
	}

private:
	PointerType ptr{};
};