#pragma once
#include<stdexcept>
#include<string>

using Color = bool;
using Direction = bool;

constexpr auto NIL = nullptr;
constexpr Color RED = 0;
constexpr Color BLACK = 1;

constexpr Direction LEFT = 0;
constexpr Direction RIGHT = 1;

template <class T = long>
class Node {
private:
	T data = T();
	Node<T>* child[2];
	Node<T>* parent = NIL;
	Color color = BLACK;
public:
	Node() {
		child[0] = child[1] = NIL;
	}

	template<class T = long>
	Node(T&& data, Color color = BLACK, Node* parent = NIL, Node* leftChild = NIL, Node* rightChild = NIL) :
		data(std::forward<T>(data)),
		parent(parent),
		color(color) {
		child[LEFT] = leftChild;
		child[RIGHT] = rightChild;
	}

	~Node() {
		~T();
	}

	Node(const Node& node) = delete;
	Node<T>& operator=(const Node& node) = delete;

	/// @brief Returns true if the both subtrees are equal
	bool operator==(const Node<T>& right) const {
		return compareSubTrees(this, &right);
	}

private:
	/// @brief iterates over nodes and check if two trees are equal.
	bool compareSubTrees(const Node<T>* left, const Node<T>* right) const {
		if (left == right)
			return true;
		if (left == NIL || right == NIL)
			return false;
		return (left->data == right->data && left->color == right->color) &&
			(compareSubTrees(left->getChild(LEFT), right->getChild(LEFT))) &&
			(compareSubTrees(left->getChild(RIGHT), right->getChild(RIGHT)));
	}

public:

	/// @brief Returns true if left subtree and right subtree are not equal.
	bool operator!=(const Node<T>& right) const {
		return !((*this) == right);
	}

	/// @brief operator that returns data and color of node as stream
	friend std::ostream& operator<<(std::ostream& os, const Node& node) {
		std::string c = node.getColor() ? "BLACK" : "RED";
		os << "Data: " << node.data << " Color: " << c << ' ' << '\n';
		return os;
	}

	/// @brief operator that reads data and color of node as stream
	friend std::istream& operator>>(std::istream& is, Node& node) {
		is >> node.data;
		is >> node.color;
		return is;
	}

	T& getData() {
		return data;
	}

	const T& getData() const {
		return data;
	}

	Color getColor() const noexcept {
		return color;
	}

	const Node<T>* getParent() const noexcept {
		return parent;
	}

	Node<T>* getParent() noexcept {
		return parent;
	}

	const Node<T>* getChild(Direction dir) const noexcept {
		return dir == LEFT ? child[LEFT] : child[RIGHT];
	}

	Node<T>* getChild(Direction dir) noexcept {
		return dir == LEFT ? child[LEFT] : child[RIGHT];
	}

	void setChild(Direction dir, Node<T>* newChild) noexcept {
		child[dir] = newChild;
	}

	void setParent(Node<T>* newParent) noexcept {
		parent = newParent;
	}

	template<class T>
	void setData(T&& newData) {
		data = std::forward<T>(newData);
	}

	void setColor(Color newColor) noexcept {
		color = newColor;
	}

	/// @brief Returns true if the both children of current node are NIL
	bool isLeaf() const noexcept {
		return child[LEFT] == NIL && child[RIGHT] == NIL;
	}

	/// @brief Returns true if the current node has not a parent
	bool isRoot() const noexcept {
		return parent == NIL;
	}

	/// @brief Returns pointer the parent of the parent of current node
	Node<T>* getGrandParent() noexcept {
		Node<T>* parent = getParent();
		if (parent == NIL)
			return NIL;
		return parent->getParent();
	}

	/// @brief Returns const the parent of the parent of current node
	const Node<T>* getGrandParent() const noexcept {
		const Node<T>* parent = getParent();
		if (parent == NIL)
			return NIL;
		return parent->getParent();
	}

	/// @brief returns if current node is left or right child
	Direction childDir() const {
		if (isRoot())
			throw std::invalid_argument("Root has not a child direction.");
		return this == (parent)->getChild(RIGHT) ? RIGHT : LEFT;
	}

	/// @brief returns sibling of current node if there is such
	const Node<T>* getSibling() const {
		const Node<T>* parent = getParent();
		if (parent == NIL)
			return NIL;
		return parent->getChild(!childDir());
	}

	/// @brief returns other child of parent of current node if there is such
	Node<T>* getSibling() {
		Node<T>* parent = getParent();
		if (parent == NIL)
			return NIL;
		return parent->getChild(!childDir());
	}

	/// @brief returns brother of father if there is such
	Node<T>* getUncle() {
		Node<T>* parent = getParent();
		if (parent == NIL)
			return NIL;
		return parent->getSibling();
	}

	/// @brief returns brother of father if there is such
	const Node<T>* getUncle() const {
		const Node<T>* parent = getParent();
		if (parent == NIL)
			return NIL;
		return parent->getSibling();
	}

	/// @brief returns nephew that is closer to current node, if there is such
	Node<T>* getCloseNephew() {
		Node<T>* sibling = getSibling();
		if (sibling == NIL)
			return NIL;
		return sibling->getChild(childDir());
	}

	/// @brief returns nephew that is closer to current node, if there is such
	const Node<T>* getCloseNephew() const {
		const Node<T>* sibling = getSibling();
		if (sibling == NIL)
			return NIL;
		return sibling->getChild(childDir());
	}

	/// @brief returns nephew that is distant to current node, if there is such
	const Node<T>* getFarNephew() const {
		const Node<T>* sibling = getSibling();
		if (sibling == NIL)
			return NIL;
		return sibling->getChild(!childDir());
	}

	/// @brief returns nephew that is distant to current node, if there is such
	Node<T>* getFarNephew() {
		Node<T>* sibling = getSibling();
		if (sibling == NIL)
			return NIL;
		return sibling->getChild(!childDir());
	}
};