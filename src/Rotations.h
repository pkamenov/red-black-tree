#pragma once
#include "RBTree.h"
#include<cassert>

template <class T, class Allocator = SimpleAllocator<Node<T>>>
class RBTree;

/// @brief static Container class with function for rebalancing and rotate 
///red-black tree after insertion and deletion
template<class T = long, class Allocator = SimpleAllocator<Node<T>>>
class Rotations {
public:
	/// @brief Function that rotates subtree defined of rootOfSubtree to direction = dir.
	static Node<T>* rotateDir(RBTree<T, Allocator>& tree, Node<T>* rootOfSubtree, Direction dir) {
		Node<T>* G = rootOfSubtree->getParent();
		Node<T>* S = rootOfSubtree->getChild(!dir);
		if (S == NIL)
			throw std::invalid_argument("Right child of rootOfSubtree must not be NIL!");
		Node<T>* C = S->getChild(dir);
		rootOfSubtree->setChild(!dir, C);
		if (C != NIL)
			C->setParent(rootOfSubtree);
		S->setChild(dir, rootOfSubtree);
		rootOfSubtree->setParent(S);
		S->setParent(G);
		if (G != NIL)
			G->setChild((rootOfSubtree == G->getChild(RIGHT) ? RIGHT : LEFT), S);
		else
			tree.setRoot(S);
		return S; // new root of subtree
	}

	/// @brief Function that rotates subtree defined of rootOfSubtree to left.
	static Node<T>* rotateLeft(RBTree<T, Allocator>& tree, Node<T>* rootOfSubtree) {
		rotateDir(tree, rootOfSubtree, LEFT);
	}

	/// @brief Function that rotates subtree defined of rootOfSubtree to right.
	static Node<T>* rotateRight(RBTree<T, Allocator>& tree, Node<T>* rootOfSubtree) {
		rotateDir(tree, rootOfSubtree, RIGHT);
	}

	/// @brief Function that rebalance the tree after insert
	/// @param tree that's being rebalanced
	/// @param newNode newly inserted node
	static void rebalance(RBTree<T, Allocator>& tree, Node<T>* currNode) {
		Node<T>* parent = currNode->getParent();
		Node<T>* grandParent = parent->getParent();
		//we insert a red node. We can only break now property if our parent is red
		do {
			if (parent->getColor() == BLACK) {
				//Case1. Parent is black. Everything is okay. Nothing to do.
				return;
			}
			//From now on parent is red. We broke the third property.
			if ((grandParent = parent->getParent()) == NIL) {
				case4(parent);
				return;
			}
			//From now on parent is red and grandParent is not NIL.
			Direction parentDirection;
			try {
				parentDirection = parent->childDir();
			}
			catch (...) {
				printf("rebalance!");
			}

			Node<T>* uncle = currNode->getUncle();
			if (uncle == NIL || uncle->getColor() == BLACK) {
				case5(tree, currNode, parent);
			}
			else {
				//if Uncle is red
				case2(parent, grandParent, uncle);
			}
			currNode = grandParent;
		} while ((parent = currNode->getParent()) != NIL);
		//p is NIL -> rebalance is complete
	}

private:
	/// @brief Case 4: The parent is red and root. We need to make it black and we are done.
	static void case4(Node<T>* parent) {
		parent->setColor(BLACK);
		return;
	}

	/// @brief Case 5: Parent is red but the uncle is black. But if newNode is inner grandChild then we need to rotate and call case6
	///Right child of a left child or left child of a right child. 
	static void case5(RBTree<T, Allocator>& tree, Node<T>* currNode, Node<T>* parent) {
		Direction dir = parent->childDir();
		Node<T>* grandParent = parent->getParent();
		if (currNode == parent->getChild(!dir)) {
			rotateDir(tree, parent, dir);
			currNode = parent; // new current node
			parent = grandParent->getChild(dir); // new parent of N
		}
		case6(tree, currNode, parent);
	}

	/// @brief Case 6: Now current newNode is an outer grandchild. Parent is red and uncle is black.
	static void case6(RBTree<T, Allocator>& tree, Node<T>* currNode, Node<T>* parent) {
		Node<T>* grandParent = parent->getParent();
		Direction dir = parent->childDir();
		rotateDir(tree, grandParent, !dir);
		parent->setColor(BLACK);
		grandParent->setColor(RED);
		return;
	}

	/// @brief Parent and uncle are red. Repaint both of them black and paint grandparent red.
	static void case2(Node<T>* parent, Node<T>* grandParent, Node<T>* uncle) {
		parent->setColor(BLACK);
		uncle->setColor(BLACK);
		grandParent->setColor(RED);
	}

public:
	/// @brief Main functions for rebalancing the tree before deallocation of node for deletion.
	static void eraseRebalance(RBTree<T, Allocator>& tree, Node<T>*& nodeForDeletion) {
		//Simple case 1
		if (tree.size() == 1) {
			tree.setRoot(NIL);
			return;
		}
		// node has two children
		if (nodeForDeletion->getChild(LEFT) != NIL && nodeForDeletion->getChild(RIGHT) != NIL) {
			simpleCase2(tree, nodeForDeletion);
		}
		// If node has one child
		if (nodeForDeletion->getChild(LEFT) == NIL || nodeForDeletion->getChild(RIGHT) == NIL) {
			if (nodeForDeletion->getColor() == RED) {
				simpleCase3(tree, nodeForDeletion);
				return; //no need of further rebalancing
			}
			else {
				//node is black
				simpleCase4(tree, nodeForDeletion);
				// we need to continue to complex cases
			}
		}
		if (nodeForDeletion->getParent() != NIL &&
			nodeForDeletion->isLeaf() &&
			nodeForDeletion->getColor() == BLACK) {
			deleteBlackNonRootLeaf(tree, nodeForDeletion);
		}
	}
private:
	/// @brief Two non-nil children
	static void simpleCase2(RBTree<T, Allocator>& tree, Node<T>*& nodeForDeletion) {
		Node<T>* leftSubTree = nodeForDeletion->getChild(LEFT);
		Node<T>* maxInRight = tree.getRightMostLeaf(leftSubTree);
		swapNodes(tree, nodeForDeletion, maxInRight);
	}

	/// @brief Deleting a red node without children. Simply deallocate the node and reallocate parent of it;
	static void simpleCase3(RBTree<T, Allocator>& tree, Node<T>* nodeForDeletion) {
		assert((nodeForDeletion->getColor() == RED &&
			nodeForDeletion->getChild(LEFT) == NIL &&
			nodeForDeletion->getChild(RIGHT) == NIL));
		Node<T>* parent = nodeForDeletion->getParent();
		Direction dir = nodeForDeletion->childDir();
		if (parent != NIL)
			parent->setChild(dir, NIL);
		nodeForDeletion->setParent(NIL);
	}

	/// @brief Node is black and has one red child or zero children.
	static void simpleCase4(RBTree<T, Allocator>& tree, Node<T>* nodeForDeletion) {
		assert(nodeForDeletion->getColor() == BLACK);
		assert(nodeForDeletion->getChild(LEFT) == NIL || nodeForDeletion->getChild(RIGHT) == NIL);
		Node<T>* child = nodeForDeletion->getChild(LEFT) != NIL ?
			nodeForDeletion->getChild(LEFT) :
			nodeForDeletion->getChild(RIGHT);
		assert(child == NIL || child->getColor() == RED);

		if (child != NIL) {
			Direction childDir = child->childDir();
			Direction nDir;
			Node<T>* nParent = nodeForDeletion->getParent();
			if (nParent != NIL)
				nDir = nodeForDeletion->childDir();
			child->setColor(BLACK);
			child->setParent(nParent);
			if (nParent != NIL) {
				nParent->setChild(nDir, child);
			}
			else {
				tree.setRoot(child);
			}

			nodeForDeletion->setChild(childDir, NIL);
			nodeForDeletion->setParent(NIL);
		}
		// we need to continue to complex cases
	}

public:
	/// @brief If after simple cases node for deletion is black,
	///non root of the tree and is leaf, we proceed to this function.
	static void deleteBlackNonRootLeaf(RBTree<T, Allocator>& tree, Node<T>* nodeForDeletion) {
		assert(nodeForDeletion->isLeaf() &&
			nodeForDeletion->getColor() == BLACK &&
			tree.getRoot() != nodeForDeletion);
		Node<T>* parent = nodeForDeletion->getParent();
		Node<T>* sibling, * closeNephew, * distantNephew;
		Direction dir = nodeForDeletion->childDir();
		parent->setChild(dir, NIL);
		do {
			sibling = parent->getChild(!dir);
			distantNephew = sibling == NIL ? NIL : sibling->getChild(!dir);
			closeNephew = sibling == NIL ? NIL : sibling->getChild(dir);
			if (sibling != NIL && sibling->getColor() == RED) {
				deleteCase3(tree, nodeForDeletion, parent, sibling,
					closeNephew, distantNephew, dir);// S red ===> P+C+D black
				return;
			}
			if (distantNephew != NIL && distantNephew->getColor() == RED) {
				deleteCase6(tree, sibling, parent, distantNephew, dir);
				return;
			}
			if (closeNephew != NIL && closeNephew->getColor() == RED) {
				deleteCase5(tree, sibling, closeNephew, distantNephew, parent, dir);
				return;
			}
			//both nephew are NIL(first iteration) or black
			if (parent->getColor() == RED) {
				deleteCase4(sibling, parent);
				return;
			}

			//case1- parent, both nephew and sibling are black
			if (sibling != NIL)
				sibling->setColor(RED);
			nodeForDeletion = parent;
			if (nodeForDeletion->getParent() == NIL)
				break;
			dir = nodeForDeletion->childDir();

		} while ((parent = nodeForDeletion->getParent()) != NIL);
		//Case 2:parent == NIL, so nodeForDeletion is the root
		return;
	}
private:
	/// @brief The sibling is red, so parent and the close and far nephews have to be black. 
	static void deleteCase3(RBTree<T, Allocator>& tree, Node<T>* nodeForDeletion, Node<T>* parent, Node<T>* sibling,
		Node<T>* closeNephew, Node<T>* distantNephew, Direction dir) {
		rotateDir(tree, parent, dir);
		assert(sibling != NIL);
		parent->setColor(RED);
		sibling->setColor(BLACK);
		sibling = closeNephew;
		distantNephew = sibling->getChild(!dir);
		if (distantNephew != NIL && distantNephew->getColor() == RED) {
			deleteCase6(tree, sibling, parent, distantNephew, dir);
			return;
		}
		closeNephew = sibling->getChild(dir);
		if (closeNephew != NIL && closeNephew->getColor() == RED) {
			deleteCase5(tree, sibling, closeNephew, distantNephew, parent, dir);
			return;
		}
		deleteCase4(sibling, parent);
	}

	/// @brief The sibling and sibling's children are black, but parent is black. 
	static void deleteCase4(Node<T>* sibling, Node<T>* parent) {
		if (sibling != NIL)
			sibling->setColor(RED);
		parent->setColor(BLACK);
	}

	/// @brief The sibling S is black, S�s close child C is red, and S�s distant child D is black.
	static void deleteCase5(RBTree<T, Allocator>& tree, Node<T>* sibling, Node<T>* closeNephew,
		Node<T>* distantNephew, Node<T>* parent, Direction dir) {
		rotateDir(tree, sibling, !dir);
		sibling->setColor(RED);
		closeNephew->setColor(BLACK);
		distantNephew = sibling;
		sibling = closeNephew;
		deleteCase6(tree, sibling, parent, distantNephew, dir);
	}

	/// @brief The sibling S is black, S�s distant child D is red.
	static void deleteCase6(RBTree<T, Allocator>& tree, Node<T>* sibling, Node<T>* parent,
		Node<T>* distantNephew, Direction dir) {
		rotateDir(tree, parent, dir);
		sibling->setColor(parent->getColor());
		parent->setColor(BLACK);
		distantNephew->setColor(BLACK);
	}


public:
	/// @brief  Deallocate the old nodes x and y. Allocate new memory for 
	///them and swap nodes without changing the color.
	/// For example, if you swap a red node with a black, then at the old position of red node is
	/// the black one, but it's red and vice verse.
	static void swapNodes(RBTree<T, Allocator>& tree, Node<T>*& x, Node<T>*& y) {
		Node<T>* newX = tree.allocator.allocate(1);
		Node<T>* newY = tree.allocator.allocate(1);
		newX = new (newX) Node<T>(y->getData(), x->getColor(), x->getParent(), x->getChild(LEFT), x->getChild(RIGHT));
		newY = new (newY) Node<T>(x->getData(), y->getColor(), y->getParent(), y->getChild(LEFT), y->getChild(RIGHT));
		Node<T>* xParent = x->getParent();
		Node<T>* yParent = y->getParent();

		// Fix up parent of x
		if (xParent != NIL) {
			xParent->setChild(x->childDir(), newX);
		}
		// Fit up parent of y, also
		if (yParent != NIL) {
			yParent->setChild(y->childDir(), newY);
		}


		if (newX->getChild(RIGHT) == y) {
			// If y was a right child of x, update reference to newX
			newX->setChild(RIGHT, newY);
		}
		if (newX->getChild(LEFT) == y) {
			// If y was a left child of x, update reference to newX
			newX->setChild(LEFT, newY);
		}

		if (newY->getChild(RIGHT) == x) {
			// If x was a right child of y, update reference to newY
			newY->setChild(RIGHT, newX);
		}
		if (newY->getChild(LEFT) == x) {
			// If x was a left child of y, update reference to newY
			newY->setChild(LEFT, newX);
		}

		// Update child references to be orphaned to point to new parents for x
		if (x->getChild(LEFT) != NIL)
			x->getChild(LEFT)->setParent(newX);
		if (x->getChild(RIGHT) != NIL)
			x->getChild(RIGHT)->setParent(newX);

		// Update child references to be orphaned to point to new parents for y
		if (y->getChild(LEFT) != NIL)
			y->getChild(LEFT)->setParent(newY);
		if (y->getChild(RIGHT) != NIL)
			y->getChild(RIGHT)->setParent(newY);

		//Change root of the tree if it's neccessary
		if (tree.getRoot() == x) {
			tree.setRoot(newX);
		}
		else if (tree.getRoot() == y) {
			tree.setRoot(newY);
		}

		// Finally deallocate old pointers and replace them with new one.
		tree.deallocateNode(x);
		tree.deallocateNode(y);
		x = newY;
		y = newX;
	}


public:

	Rotations() = delete;
	Rotations(const Rotations<T, Allocator>& other) = delete;
	Rotations& operator=(const Rotations<T, Allocator>& other) = delete;
};