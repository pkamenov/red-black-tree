#pragma once
#include "Node.h"
#include<vector>
template<class T = long>
class RBTreeHelper {
public:
	/// @brief  check 3rd invariant of red-black tree. It says that all red nodes must have black children
	static bool allRedNodesHasBlackChildren(const Node<T>* root) noexcept {
		if (root == NIL) {
			return true;
		}
		if (root->getColor() == RED) {
			if ((root->getChild(LEFT) != NIL && root->getChild(LEFT)->getColor() == RED) ||
				(root->getChild(RIGHT) != NIL && root->getChild(RIGHT)->getColor() == RED)) {
				return false;
			}
		}
		return allRedNodesHasBlackChildren(root->getChild(LEFT)) &&
			allRedNodesHasBlackChildren(root->getChild(RIGHT));
	}

	/// @brief check 4th invariant of red-black tree. It says that every path of black node to his leafs should contains equal number black nodes
	static bool everyPathToNILHasSameNumberOfBlackNodes(const Node<T>* root) {
		std::vector<size_t> count;
		std::size_t currentCount = 0;
		std::size_t i = 0;
		everyPathToNILHasSameNumberOfBlackNodes(root, count, currentCount, i);

		for (std::size_t j = 1; j < count.size(); ++j) {
			if (count[j] != count[j - 1])
				return false;
		}
		return true;
	}

private:
	/// @brief Private member function used by public wrapper.
	static void everyPathToNILHasSameNumberOfBlackNodes(const Node<T>* root, std::vector<std::size_t>& vectorOfCounts,
		std::size_t currentCount, std::size_t& i) {
		if (root == NIL) {
			vectorOfCounts.emplace_back(currentCount);
			return;
		}
		if (root->getColor() == BLACK) {
			++currentCount;
		}
		everyPathToNILHasSameNumberOfBlackNodes(root->getChild(LEFT), vectorOfCounts, currentCount, i);
		everyPathToNILHasSameNumberOfBlackNodes(root->getChild(RIGHT), vectorOfCounts, currentCount, i);
	}

public:
	/// @brief function that concatenates 3d and 4th invariant and check if given tree is valid red-black tree 
	static bool isRedBlackTree(const Node<T>* root) {
		if (root == NIL)
			return true;
		return allRedNodesHasBlackChildren(root) && everyPathToNILHasSameNumberOfBlackNodes(root);
	}
};