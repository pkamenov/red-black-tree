#pragma once
#include"Node.h"
#include"SimpleAllocator.h"
#include <vector>
#include"Rotations.h"
#include"RBTreeIterator.h"

template <class T = long, class Allocator = SimpleAllocator<Node<T>>>
class RBTree {
private:
	Node<T>* root = NIL;
	Allocator allocator = Allocator();
public:
	using ValueType = Node<T>;
	using Iterator = RBTreeIterator<RBTree<T, Allocator>>;
	friend Rotations<T, Allocator>;

	RBTree() = default;

	template<class Y = T>
	RBTree(Y&& value) {
		root = allocator.allocate(1);
		root = new (root) Node<T>(std::forward<Y>(value), RED);
	}

	template<class ... Args>
	RBTree(Args&& ... args) {
		insert(T(std::forward<Args>(args)...));
	}

	const Node<T>* getRoot() const {
		return root;
	}

private:
	void setRoot(Node<T>* newRoot) {
		root = newRoot;
	}
public:

	RBTree(const RBTree& other) = delete;
	RBTree& operator=(const RBTree& other) = delete;

	~RBTree() {
		deleteSubtree(root);
	}

private:
	/// @brief function that deallocates memory and delete elements in given subtree
	void deleteSubtree(Node<T>* rootOfSubtree) {
		if (rootOfSubtree == NIL)
			return;
		deleteSubtree(rootOfSubtree->getChild(LEFT));
		deleteSubtree(rootOfSubtree->getChild(RIGHT));
		allocator.deallocate(rootOfSubtree, 1);
	}

public:

	const Allocator& get_allocator() const noexcept {
		return allocator;
	}

	/// @brief Returns Iterator to first element.
	Iterator begin() {
		return Iterator(getLeftMostLeaf(root));
	}

	/// @brief Returns Iterator to imaginary element after last.
	Iterator end() {
		return Iterator(NIL);
	}

	/// @brief Returns iterator to last element 
	Iterator rbegin() {
		return Iterator(getRightMostLeaf(root));
	}

	/// @brief Returns Iterator to imaginary element before first.
	Iterator rend() {
		return Iterator(NIL);
	}

	/// @brief Returns const Iterator to first element.
	const Iterator cbegin() {
		return Iterator(getLeftMostLeaf(root));
	}

	/// @brief Returns const Iterator to imaginary element after last.
	const Iterator cend() {
		return Iterator(NIL);
	}

	/// @brief Returns const iterator to last element 
	const Iterator crbegin() {
		return Iterator(getRightMostLeaf(root));
	}

	/// @brief Returns const Iterator to imaginary element before first.
	const Iterator crend() {
		return Iterator(NIL);
	}


private:
	/// @brief returns the smallest element in subtree
	Node<T>* getLeftMostLeaf(Node<T>* curRoot) {
		if (curRoot == NIL)
			return curRoot;
		if (curRoot->getChild(LEFT) == NIL)
			return curRoot;
		return getLeftMostLeaf(curRoot->getChild(LEFT));
	}

	/// @brief returns the largest element in the subtree
	Node<T>* getRightMostLeaf(Node<T>* curRoot) {
		if (curRoot == NIL)
			return curRoot;
		if (curRoot->getChild(RIGHT) == NIL)
			return curRoot;
		return getRightMostLeaf(curRoot->getChild(RIGHT));
	}

public:

	/// @brief delete node by given value and clear allocated memory
	void erase(const T& value) {
		Iterator toDelete = find(value);
		erase(toDelete);
	}

	/// @brief delete node by given iterator and clear allocated memory
	void erase(Iterator it) {
		if (empty())
			throw std::length_error("Can't erase form and empty tree.");
		Node<T>* toDelete = &(*it);
		if (toDelete == NIL)
			return;
		Rotations<T, Allocator>::eraseRebalance((*this), toDelete);
		deallocateNode(toDelete);
	}

	/// @brief check if node with given value is in the tree
	bool contains(const T& value) const {
		return contains(root, value) != NIL;
	}

	/// @brief returns iterator to element with given value
	const Iterator find(const T& value) const {
		return Iterator(contains(root, value));
	}

	/// @brief returns iterator to element with given value
	Iterator find(const T& value) {
		return Iterator(contains(root, value));
	}

private:

	const Node<T>* contains(Node<T>* rootOfSubtree, const T& value) const {
		if (rootOfSubtree == NIL)
			return NIL;
		if (rootOfSubtree->getData() == value)
			return rootOfSubtree;
		const Node<T>* leftResult = contains(rootOfSubtree->getChild(LEFT), value);
		if (leftResult != NIL)
			return leftResult;
		return contains(rootOfSubtree->getChild(RIGHT), value);
	}

	Node<T>* contains(Node<T>* rootOfSubtree, const T& value) {
		return const_cast<Node<T>*>(const_cast<const RBTree*>(this)->contains(rootOfSubtree, value));
	}

public:
	/// @brief deallocate whole tree
	void clear() {
		deleteSubtree(root);
		root = NIL;
	}

	/// @brief check if there aren't elements in tree
	bool empty() const noexcept {
		return root == NIL;
	}

	/// @brief Returns the number of nodes in the current RBTree
	std::size_t size() const noexcept {
		return size(root);
	}

private:
	std::size_t size(Node<T>* rootOfSubtree) const {
		if (rootOfSubtree == NIL)
			return 0;
		return size(rootOfSubtree->getChild(LEFT)) + size(rootOfSubtree->getChild(RIGHT)) + 1;
	}

	void deallocateNode(Node<T>* n) {
		allocator.deallocate(n, 1);
	}
public:
	/// @brief return number of levels of tree. Empty tree has level -1
	long height() const {
		return height(root, -1);
	}

private:
	long height(Node<T>* rootOfSubtree, long curHeight) const {
		if (rootOfSubtree == NIL)
			return curHeight;
		return std::max(height(rootOfSubtree->getChild(LEFT), curHeight + 1),
			height(rootOfSubtree->getChild(RIGHT), curHeight + 1));
	}

public:
	/// @brief Insert given value to the right position in the ordered tree and do some rebalance if necessary to bring back tree to balance
	template <class Y = T, class A = Allocator>
	Iterator insert(Y&& value) {
		if (empty()) {
			root = allocator.allocate(1);
			root = new (root) Node<T>(std::forward<Y>(value), RED);
			//There is no need for rebalance in this case
			return Iterator(root);
		}
		else {
			Iterator newNode = insert(root, std::forward<Y>(value));
			Rotations<T, Allocator>::rebalance(*(this), &(*newNode));
			return newNode;
		}
	}

	/// @brief Emplace object build in place from arguments given in function  given value to the 
	///right position in the ordered tree and do some rebalance if necessary to bring back tree to balance
	template<class ... Args>
	Iterator emplace(Args&& ... args) {
		return insert(T(std::forward<Args>(args)...));
	}

private:
	template <class Y = T, class A = Allocator>
	Iterator insert(Node<T>* rootOfSubtree, Y&& value) {
		if (value < rootOfSubtree->getData()) {
			if (rootOfSubtree->getChild(LEFT) == NIL) {
				Node<T>* node = allocator.allocate(1);
				node = new (node) Node<T>(std::forward<Y>(value), RED, rootOfSubtree);
				rootOfSubtree->setChild(LEFT, node);
				return Iterator(node);
			}
			else {
				return insert(rootOfSubtree->getChild(LEFT), std::forward<Y>(value));
			}
		}
		else {
			if (rootOfSubtree->getChild(RIGHT) == NIL) {
				Node<T>* node = allocator.allocate(1);
				node = new (node) Node<T>(std::forward<Y>(value), RED, rootOfSubtree);
				rootOfSubtree->setChild(RIGHT, node);
				return Iterator(node);
			}
			else {
				return insert(rootOfSubtree->getChild(RIGHT), std::forward<T>(value));
			}
		}
	}

public:
	/// @brief makes sorted vector with elements of the tree
	std::vector<T> toSortedVector() const  noexcept {
		std::vector<T> result;
		toSortedVector(root, result);
		return result;
	}

private:
	void toSortedVector(Node<T>* rootOfSubtree, std::vector<T>& vec) const noexcept {
		if (rootOfSubtree != NIL) {
			toSortedVector(rootOfSubtree->getChild(LEFT), vec);
			vec.push_back(rootOfSubtree->getData());
			toSortedVector(rootOfSubtree->getChild(RIGHT), vec);
		}
	}
};