# Red-black tree

Red-black tree implementation developed during masterclass-2021 DigitalLights.
Rotations and rebalancing of the tree are based on the logic explained in Wikipedia page for red-black trees.
 https://en.wikipedia.org/wiki/Red–black_tree
You can fina a PDF of the page in documentation folder.

Project tests depend ot Catch2.
